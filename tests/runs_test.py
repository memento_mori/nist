import math

import numpy as np

from . import GeneratorTestResult
from .abstract import AbstractTest


class RunsTest(AbstractTest):
    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        proportion_of_ones: float = np.count_nonzero(sequence) / len(sequence)
        observed_runs: float = 1

        for i in range(len(sequence) - 1):
            if sequence[i] != sequence[i + 1]:
                observed_runs += 1

        score: float = 0
        if proportion_of_ones != 1:
            score: float = math.erfc(
                abs(observed_runs - (2.0 * len(sequence) * proportion_of_ones * (1 - proportion_of_ones))) / (
                        2.0 * math.sqrt(2.0 * len(sequence)) * proportion_of_ones * (1 - proportion_of_ones)))

        if score >= self.threshold:
            return GeneratorTestResult(True, score)

        return GeneratorTestResult(False, score)
