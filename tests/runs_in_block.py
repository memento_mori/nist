import numpy as np
from scipy.special import gammaincc

from .abstract import AbstractTest
from .result import GeneratorTestResult


def get_constant_probabilities(size_of_block: int, index: int) -> float:
    if size_of_block == 8:
        return [0.2148, 0.3672, 0.2305, 0.1875][index]
    elif size_of_block == 128:
        return [0.1174, 0.2430, 0.2493, 0.1752, 0.1027, 0.1124][index]
    elif size_of_block == 512:
        return [0.1170, 0.2460, 0.2523, 0.1755, 0.1027, 0.1124][index]
    elif size_of_block == 1000:
        return [0.1307, 0.2437, 0.2452, 0.1714, 0.1002, 0.1088][index]
    else:
        return [0.0882, 0.2092, 0.2483, 0.1933, 0.1208, 0.0675, 0.0727][index]


class RunsInBlock(AbstractTest):
    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        if len(sequence) < 128:
            raise ValueError('Too short sequence')

        block_size: int = 10000
        if len(sequence) < 6272:
            block_size: int = 8
        elif len(sequence) < 750000:
            block_size: int = 128

        k: int = 6
        blocks_number: int = 75
        if block_size == 8:
            k: int = 3
            blocks_number: int = 16
        elif block_size == 128:
            k: int = 5
            blocks_number: int = 49

        frequencies: np.ndarray = np.zeros(7, dtype=int)

        for i in range(blocks_number):
            block: np.ndarray = sequence[i * block_size:((i + 1) * block_size)]
            run_length: int = 0
            longest_run_length: int = 0

            for j in range(block_size):  # find longest run length
                if block[j] == 1:
                    run_length += 1
                    if run_length > longest_run_length:
                        longest_run_length = run_length
                else:
                    run_length = 0

            if block_size == 8:
                frequencies[min(3, max(0, longest_run_length - 1))] += 1
            elif block_size == 128:
                frequencies[min(5, max(0, longest_run_length - 4))] += 1
            else:
                frequencies[min(6, max(0, longest_run_length - 10))] += 1

        chi_square: float = 0.0

        for i in range(k + 1):
            probability_constant: float = get_constant_probabilities(block_size, i)
            chi_square += ((frequencies[i] - blocks_number * probability_constant) ** 2) / (
                    blocks_number * probability_constant)

        score: float = gammaincc(k / 2.0, chi_square / 2.0)

        if score >= self.threshold:
            return GeneratorTestResult(True, score)

        return GeneratorTestResult(False, score)
