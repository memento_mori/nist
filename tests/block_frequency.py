import numpy as np
from scipy.special import gammaincc

from .abstract import AbstractTest
from .result import GeneratorTestResult


class BlockFrequency(AbstractTest):

    def __init__(self, block_size: int = 100):
        self._block_size = block_size

    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        block_size: int = self._block_size
        blocks_number: int = int(sequence.size // block_size)

        block_fractions: np.ndarray = np.zeros(blocks_number, dtype=float)
        for i in range(blocks_number):
            block: np.ndarray = sequence[i * block_size:((i + 1) * block_size)]
            block_fractions[i] = np.count_nonzero(block) / block_size

        chi_square: float = sum(4.0 * block_size * ((block_fractions[:] - 0.5) ** 2))

        score: float = gammaincc((blocks_number / 2.0), chi_square / 2.0)

        if score >= self.threshold:
            return GeneratorTestResult(True, score)

        return GeneratorTestResult(False, score)
