import math

import numpy as np

from . import GeneratorTestResult
from .abstract import AbstractTest


class SpectralTest(AbstractTest):
    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray):
        if (sequence.size % 2) == 1:
            sequence = sequence[:-1]
        sequence[sequence == 0] = -1
        discrete_fourier_transform = np.fft.fft(sequence)
        magnitudes = abs(discrete_fourier_transform)[:sequence.size // 2]
        threshold: float = math.sqrt(math.log(1.0 / 0.05) * sequence.size)
        expected_peaks: float = 0.95 * sequence.size / 2.0
        counted_peaks: float = float(len(magnitudes[magnitudes < threshold]))
        normalized_difference: float = (counted_peaks - expected_peaks) / math.sqrt((sequence.size * 0.95 * 0.05) / 4)
        score: float = math.erfc(abs(normalized_difference) / math.sqrt(2))

        if score >= self.threshold:
            return GeneratorTestResult(True, score)
        return GeneratorTestResult(False, score)
