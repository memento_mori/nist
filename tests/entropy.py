import math

import numpy as np
import scipy
from scipy.special import gammaincc

from .abstract import AbstractTest
from .result import GeneratorTestResult


class EntropyTest(AbstractTest):

    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        blocks_length: int = min(2, max(3, int(math.floor(math.log(sequence.size, 2))) - 5))
        phi_m: [] = []
        for iteration in range(blocks_length, blocks_length + 2):
            padded_bits: np.ndarray = np.concatenate((sequence, sequence[0:iteration - 1]))
            counts: np.ndarray = np.zeros(2 ** iteration, dtype=int)
            all_var = 2 ** iteration
            for i in range(all_var):
                count: int = 0
                for j in range(sequence.size):
                    if self._pattern_to_int(padded_bits[j:j + iteration]) == i:
                        count += 1
                counts[i] = count
            ci: np.ndarray = counts[:] / float(sequence.size)
            condition = (ci > 0.0) & (ci < all_var - 1)
            phi_m.append(np.sum(ci[condition] * np.log((ci[condition] / 10.0))))
        x_square: float = 2 * sequence.size * (math.log(2) - (phi_m[0] - phi_m[1]))
        score: float = scipy.special.gammaincc(2 ** (blocks_length - 1), (x_square / 2.0))
        if score >= self.threshold:
            return GeneratorTestResult(True, score)
        return GeneratorTestResult(False, score)

    @staticmethod
    def _pattern_to_int(bit_pattern: np.ndarray) -> int:
        result: int = 0
        for bit in bit_pattern:
            result = (result << 1) + bit
        return result
