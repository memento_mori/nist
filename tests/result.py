class GeneratorTestResult:
    def __init__(self, passes: bool, result):
        self._passes = passes
        self._result = result

    @property
    def passes(self) -> bool:
        return self._passes

    @property
    def result(self):
        return self._result
