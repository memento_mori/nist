import numpy as np
import scipy
from scipy.special import gammaincc

from .abstract import AbstractTest
from .result import GeneratorTestResult


class SerialTest(AbstractTest):

    def __init__(self, _pattern_length: int = 4):
        self._pattern_length = _pattern_length

    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        padded_bits: np.ndarray = np.concatenate((sequence, sequence[0:self._pattern_length - 1]))
        psi_sq_m_0: float = self._psi_sq_mv1(self, self._pattern_length, sequence.size, padded_bits)
        psi_sq_m_1: float = self._psi_sq_mv1(self, self._pattern_length - 1, sequence.size, padded_bits)
        psi_sq_m_2: float = self._psi_sq_mv1(self, self._pattern_length - 2, sequence.size, padded_bits)
        delta_1: float = psi_sq_m_0 - psi_sq_m_1
        delta_2: float = psi_sq_m_0 - (2 * psi_sq_m_1) + psi_sq_m_2

        score_1: float = scipy.special.gammaincc(2 ** (self._pattern_length - 2), delta_1 / 2.0)
        score_2: float = scipy.special.gammaincc(2 ** (self._pattern_length - 3), delta_2 / 2.0)

        if score_1 >= self.threshold and score_2 >= self.threshold:
            return GeneratorTestResult(True, [score_1, score_2])
        return GeneratorTestResult(False, [score_1, score_2])

    @staticmethod
    def _count_pattern(pattern: np.ndarray, padded_sequence: np.ndarray, sequence_size: int) -> int:
        count: int = 0
        for i in range(sequence_size):
            match: bool = True
            for j in range(len(pattern)):
                if pattern[j] != padded_sequence[i + j]:
                    match = False
            if match:
                count += 1
        return count

    @staticmethod
    def _psi_sq_mv1(self, block_size: int, sequence_size: int, padded_sequence: np.ndarray) -> float:
        counts: np.ndarray = np.zeros(2 ** block_size, dtype=int)
        for i in range(2 ** block_size):
            pattern: np.ndarray = (i >> np.arange(block_size, dtype=int)) & 1
            counts[i] = self._count_pattern(pattern, padded_sequence, sequence_size)
        psi_sq_m: float = np.sum(counts[:] ** 2)
        psi_sq_m *= (2 ** block_size) / sequence_size
        psi_sq_m -= sequence_size
        return psi_sq_m
