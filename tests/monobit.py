import math

import numpy as np

from .abstract import AbstractTest
from .result import GeneratorTestResult


class MonobitTest(AbstractTest):

    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        ones_count: int = np.count_nonzero(sequence)
        zeroes_count: int = len(sequence) - ones_count
        difference: int = abs(ones_count - zeroes_count)
        score: float = math.erfc(float(difference) / (math.sqrt(float(len(sequence))) * math.sqrt(2.0)))

        if score >= self.threshold:
            return GeneratorTestResult(True, score)

        return GeneratorTestResult(False, score)
