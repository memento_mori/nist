import numpy as np
import scipy.special

from .result import GeneratorTestResult
from .abstract import AbstractTest


class OverlappingTemplateMatchingTest(AbstractTest):
    def __init__(self):
        self._template_length: int = 10
        self._blocks_number: int = 8
        self._freedom_degrees: int = 5
        self._substring_bits_length: int = 1032

    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray):
        b_template: np.ndarray = np.ones(self._template_length, dtype=int)
        matches_distributions: np.ndarray = np.zeros(self._freedom_degrees + 1, dtype=int)
        for i in range(self._blocks_number):
            block: np.ndarray = sequence[i * self._substring_bits_length:(i + 1) * self._substring_bits_length]
            count: int = 0
            for position in range(self._substring_bits_length - self._template_length):
                bool_block = block[position:position + self._template_length] == b_template
                if not isinstance(bool_block, bool) and all(bool_block):
                    count += 1
                if isinstance(bool_block, bool) and bool_block:
                    count += 1
            matches_distributions[min(count, self._freedom_degrees)] += 1
        eta: float = (self._substring_bits_length - self._template_length + 1.0) / (
                2.0 ** self._template_length) / 2.0
        probabilities: np.ndarray = np.array([0.364091, 0.185659, 0.139381, 0.100571, 0.0704323, 0.139865])
        probabilities[:self._freedom_degrees] = self._get_probabilities(np.arange(self._freedom_degrees)[:], eta)
        probabilities[-1] = 1.0 - np.sum(probabilities)
        chi_square: float = float(np.sum(
            ((matches_distributions[:] - (self._blocks_number * probabilities[:])) ** 2) / (
                    self._blocks_number * probabilities[:])))
        if chi_square != 0:
            score: float = scipy.special.gammaincc(5.0 / 2.0, chi_square / 2.0)
            if score >= self.threshold:
                return GeneratorTestResult(True, score)
            return GeneratorTestResult(False, score)

        return GeneratorTestResult(False, 0)

    @staticmethod
    def _get_probabilities(freedom_degree_values: [], eta_value: float) -> []:
        probabilities: [] = []
        for freedom_degree_value in freedom_degree_values:
            if freedom_degree_value == 0:
                probability: float = np.exp(-eta_value)
            else:
                indexes: np.ndarray = np.arange(1, freedom_degree_value + 1)
                probability: float = float(
                    np.sum(np.exp(-eta_value - freedom_degree_value * np.log(2) + indexes[:] * np.log(eta_value)
                                  - np.log(scipy.special.gamma(indexes[:] + 1))
                                  + np.log(scipy.special.gamma(freedom_degree_value))
                                  - np.log(scipy.special.gamma(indexes[:]))
                                  - np.log(scipy.special.gamma(freedom_degree_value - indexes[:] + 1)))))

            probabilities.append(probability)
        return probabilities
