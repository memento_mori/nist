import math

import numpy as np

from .abstract import AbstractTest
from .result import GeneratorTestResult


class RandomExcursionVariant(AbstractTest):

    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        sequence_copy: np.ndarray = sequence.copy()
        sequence_copy[sequence_copy == 0] = -1
        sum_prime: np.ndarray = np.concatenate(
            (np.array([0]), np.cumsum(sequence_copy), np.array([0]))).astype(int)
        cycles: [] = []
        cycle: [] = [0]
        for index, _ in enumerate(sum_prime[1:]):
            if sum_prime[index] != 0:
                cycle += [sum_prime[index]]
            else:
                cycle += [0]
                cycles.append(cycle)
                cycle: [] = [0]
        cycles.append(cycle)
        cycles_size: int = len(cycles)
        unique, counts = np.unique(sum_prime[abs(sum_prime) < 10], return_counts=True)
        scores: [] = []
        for key, value in zip(unique, counts):
            if key != 0:
                scores.append(abs(value - cycles_size) / math.sqrt(2.0 * cycles_size * ((4.0 * abs(key)) - 2.0)))
        if all(score >= self.threshold for score in scores):
            return GeneratorTestResult(True, scores)
        return GeneratorTestResult(False, scores)
