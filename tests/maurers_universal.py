import math

import numpy as np

from .abstract import AbstractTest
from .result import GeneratorTestResult


class MaurersUniversalTest(AbstractTest):

    def __init__(self):
        self._default_pattern_size: int = 6
        self._thresholds: [] = [904960, 2068480, 4654080, 10342400, 22753280, 49643520, 107560960, 231669760, 496435200,
                                1059061760]
        self._expected_value_table: [] = [0, 0.73264948, 1.5374383, 2.40160681, 3.31122472, 4.25342659, 5.2177052,
                                          6.1962507, 7.1836656, 8.1764248, 9.1723243, 10.170032, 11.168765, 12.168070,
                                          13.167693, 14.167488, 15.167379]
        self._variance_table: [] = [0, 0.690, 1.338, 1.901, 2.358, 2.705, 2.954, 3.125, 3.238, 3.311, 3.356, 3.384,
                                    3.401, 3.410, 3.416, 3.419, 3.421]
        self._last_bits_size: int = -1
        self._pattern_length: int = -1
        self._blocks_number: int = -1
        self._q_blocks: int = -1
        self._k_blocks: int = -1

    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        if self._last_bits_size == -1 or self._last_bits_size != sequence.size:
            pattern_length: int = self._default_pattern_size
            for threshold in self._thresholds:
                if sequence.size >= threshold:
                    pattern_length += 1
            blocks_number: int = int(sequence.size // pattern_length)
            q_blocks: int = 10 * (2 ** pattern_length)
            k_blocks: int = blocks_number - q_blocks
            self._last_bits_size = sequence.size
            self._pattern_length = pattern_length
            self._blocks_number = blocks_number
            self._q_blocks = q_blocks
            self._k_blocks = k_blocks
        else:
            pattern_length: int = self._pattern_length
            blocks_number: int = self._blocks_number
            q_blocks: int = self._q_blocks
            k_blocks: int = self._k_blocks
        table: np.ndarray = np.zeros(2 ** pattern_length, dtype=int)
        for i in range(q_blocks):
            pattern: np.ndarray = sequence[i * pattern_length:(i + 1) * pattern_length]
            table[self._pattern_to_int(pattern)] = i + 1
        computed_sum: float = 0.0
        for i in range(q_blocks, blocks_number):
            pattern: np.ndarray = sequence[i * pattern_length:(i + 1) * pattern_length]
            difference: int = i + 1 - table[self._pattern_to_int(pattern)]
            table[self._pattern_to_int(pattern)] = i + 1
            computed_sum += math.log(difference, 2)
        fn: float = computed_sum / k_blocks
        magnitude: float = abs((fn - self._expected_value_table[pattern_length]) / (
                (math.sqrt(self._variance_table[pattern_length])) * math.sqrt(2)))
        score: float = math.erfc(magnitude)
        if score >= self.threshold:
            return GeneratorTestResult(True, score)
        return GeneratorTestResult(False, score)

    @staticmethod
    def _pattern_to_int(bit_pattern: np.ndarray) -> int:
        result: int = 0
        for bit in bit_pattern:
            result = (result << 1) + bit
        return result
