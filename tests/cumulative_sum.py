import math

import numpy as np

from .abstract import AbstractTest
from .result import GeneratorTestResult


class CumulativeSum(AbstractTest):

    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        sequence_copy: np.ndarray = sequence.copy()
        sequence_copy[sequence_copy == 0] = -1
        forward_sum: int = 0
        backward_sum: int = 0
        forward_max: int = 0
        backward_max: int = 0
        for i in range(sequence_copy.size):
            forward_sum += sequence_copy[i]
            backward_sum += sequence_copy[sequence_copy.size - i - 1]
            forward_max = max(abs(forward_sum), abs(forward_max))
            backward_max = max(abs(backward_sum), abs(backward_max))
        score_1: float = self.p_value(sequence_copy.size, forward_max)
        score_2: float = self.p_value(sequence_copy.size, backward_max)

        if score_1 >= self.threshold and score_2 >= self.threshold:
            return GeneratorTestResult(True, [score_1, score_2])
        return GeneratorTestResult(False, [score_1, score_2])

    @staticmethod
    def p_value(sequence_size: int, max_excursion: int) -> float:
        sum_a: float = 0.0
        start_k: int = int(math.floor((((float(-sequence_size) / max_excursion) + 1.0) / 4.0)))
        end_k: int = int(math.floor((((float(sequence_size) / max_excursion) - 1.0) / 4.0)))
        for k in range(start_k, end_k + 1):
            c: float = 0.5 * math.erfc(-(((4.0 * k) + 1.0) * max_excursion) / math.sqrt(sequence_size) * math.sqrt(0.5))
            d: float = 0.5 * math.erfc(-(((4.0 * k) - 1.0) * max_excursion) / math.sqrt(sequence_size) * math.sqrt(0.5))
            sum_a = sum_a + c - d
        sum_b: float = 0.0
        start_k = int(math.floor((((float(-sequence_size) / max_excursion) - 3.0) / 4.0)))
        end_k = int(math.floor((((float(sequence_size) / max_excursion) - 1.0) / 4.0)))
        for k in range(start_k, end_k + 1):
            c: float = 0.5 * math.erfc(-(((4.0 * k) + 3.0) * max_excursion) / math.sqrt(sequence_size) * math.sqrt(0.5))
            d: float = 0.5 * math.erfc(-(((4.0 * k) + 1.0) * max_excursion) / math.sqrt(sequence_size) * math.sqrt(0.5))
            sum_b = sum_b + c - d
        return 1.0 - sum_a + sum_b
