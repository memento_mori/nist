import random

import numpy as np
import scipy.special

from .abstract import AbstractTest
from .result import GeneratorTestResult


class NonOverlappingTemplateMatchingTest(AbstractTest):
    def __init__(self):
        self._blocks_number: int = 8
        self._templates: [] = [
            [[0, 1], [1, 0]],
            [[0, 0, 1], [0, 1, 1], [1, 0, 0], [1, 1, 0]],
            [[0, 0, 0, 1], [0, 0, 1, 1], [0, 1, 1, 1], [1, 0, 0, 0], [1, 1, 0, 0], [1, 1, 1, 0]],
            [[0, 0, 0, 0, 1], [0, 0, 0, 1, 1], [0, 0, 1, 0, 1], [0, 1, 0, 1, 1], [0, 0, 1, 1, 1], [0, 1, 1, 1, 1],
             [1, 1, 1, 0, 0], [1, 1, 0, 1, 0], [1, 0, 1, 0, 0], [1, 1, 0, 0, 0], [1, 0, 0, 0, 0], [1, 1, 1, 1, 0]],
            [[0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 1, 1], [0, 0, 0, 1, 0, 1], [0, 0, 0, 1, 1, 1], [0, 0, 1, 0, 1, 1],
             [0, 0, 1, 1, 0, 1], [0, 0, 1, 1, 1, 1], [0, 1, 0, 0, 1, 1], [0, 1, 0, 1, 1, 1], [0, 1, 1, 1, 1, 1],
             [1, 0, 0, 0, 0, 0], [1, 0, 1, 0, 0, 0], [1, 0, 1, 1, 0, 0], [1, 1, 0, 0, 0, 0], [1, 1, 0, 0, 1, 0],
             [1, 1, 0, 1, 0, 0], [1, 1, 1, 0, 0, 0], [1, 1, 1, 0, 1, 0], [1, 1, 1, 1, 0, 0], [1, 1, 1, 1, 1, 0]],
            [[0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 1, 1], [0, 0, 0, 0, 1, 0, 1], [0, 0, 0, 0, 1, 1, 1],
             [0, 0, 0, 1, 0, 0, 1], [0, 0, 0, 1, 0, 1, 1], [0, 0, 0, 1, 1, 0, 1], [0, 0, 0, 1, 1, 1, 1],
             [0, 0, 1, 0, 0, 1, 1], [0, 0, 1, 0, 1, 0, 1], [0, 0, 1, 0, 1, 1, 1], [0, 0, 1, 1, 0, 1, 1],
             [0, 0, 1, 1, 1, 0, 1], [0, 0, 1, 1, 1, 1, 1], [0, 1, 0, 0, 0, 1, 1], [0, 1, 0, 0, 1, 1, 1],
             [0, 1, 0, 1, 0, 1, 1], [0, 1, 0, 1, 1, 1, 1], [0, 1, 1, 0, 1, 1, 1], [0, 1, 1, 1, 1, 1, 1],
             [1, 0, 0, 0, 0, 0, 0], [1, 0, 0, 1, 0, 0, 0], [1, 0, 1, 0, 0, 0, 0], [1, 0, 1, 0, 1, 0, 0],
             [1, 0, 1, 1, 0, 0, 0], [1, 0, 1, 1, 1, 0, 0], [1, 1, 0, 0, 0, 0, 0], [1, 1, 0, 0, 0, 1, 0],
             [1, 1, 0, 0, 1, 0, 0], [1, 1, 0, 1, 0, 0, 0], [1, 1, 0, 1, 0, 1, 0], [1, 1, 0, 1, 1, 0, 0],
             [1, 1, 1, 0, 0, 0, 0], [1, 1, 1, 0, 0, 1, 0], [1, 1, 1, 0, 1, 0, 0], [1, 1, 1, 0, 1, 1, 0],
             [1, 1, 1, 1, 0, 0, 0], [1, 1, 1, 1, 0, 1, 0], [1, 1, 1, 1, 1, 0, 0], [1, 1, 1, 1, 1, 1, 0]],
            [[0, 0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 1, 1], [0, 0, 0, 0, 0, 1, 0, 1], [0, 0, 0, 0, 0, 1, 1, 1],
             [0, 0, 0, 0, 1, 0, 0, 1], [0, 0, 0, 0, 1, 0, 1, 1], [0, 0, 0, 0, 1, 1, 0, 1], [0, 0, 0, 0, 1, 1, 1, 1],
             [0, 0, 0, 1, 0, 0, 1, 1], [0, 0, 0, 1, 0, 1, 0, 1], [0, 0, 0, 1, 0, 1, 1, 1], [0, 0, 0, 1, 1, 0, 0, 1],
             [0, 0, 0, 1, 1, 0, 1, 1], [0, 0, 0, 1, 1, 1, 0, 1], [0, 0, 0, 1, 1, 1, 1, 1], [0, 0, 1, 0, 0, 0, 1, 1],
             [0, 0, 1, 0, 0, 1, 0, 1], [0, 0, 1, 0, 0, 1, 1, 1], [0, 0, 1, 0, 1, 0, 1, 1], [0, 0, 1, 0, 1, 1, 0, 1],
             [0, 0, 1, 0, 1, 1, 1, 1], [0, 0, 1, 1, 0, 1, 0, 1], [0, 0, 1, 1, 0, 1, 1, 1], [0, 0, 1, 1, 1, 0, 1, 1],
             [0, 0, 1, 1, 1, 1, 0, 1], [0, 0, 1, 1, 1, 1, 1, 1], [0, 1, 0, 0, 0, 0, 1, 1], [0, 1, 0, 0, 0, 1, 1, 1],
             [0, 1, 0, 0, 1, 0, 1, 1], [0, 1, 0, 0, 1, 1, 1, 1], [0, 1, 0, 1, 0, 0, 1, 1], [0, 1, 0, 1, 0, 1, 1, 1],
             [0, 1, 0, 1, 1, 0, 1, 1], [0, 1, 0, 1, 1, 1, 1, 1], [0, 1, 1, 0, 0, 1, 1, 1], [0, 1, 1, 0, 1, 1, 1, 1],
             [0, 1, 1, 1, 1, 1, 1, 1], [1, 0, 0, 0, 0, 0, 0, 0], [1, 0, 0, 1, 0, 0, 0, 0], [1, 0, 0, 1, 1, 0, 0, 0],
             [1, 0, 1, 0, 0, 0, 0, 0], [1, 0, 1, 0, 0, 1, 0, 0], [1, 0, 1, 0, 1, 0, 0, 0], [1, 0, 1, 0, 1, 1, 0, 0],
             [1, 0, 1, 1, 0, 0, 0, 0], [1, 0, 1, 1, 0, 1, 0, 0], [1, 0, 1, 1, 1, 0, 0, 0], [1, 0, 1, 1, 1, 1, 0, 0],
             [1, 1, 0, 0, 0, 0, 0, 0], [1, 1, 0, 0, 0, 0, 1, 0], [1, 1, 0, 0, 0, 1, 0, 0], [1, 1, 0, 0, 1, 0, 0, 0],
             [1, 1, 0, 0, 1, 0, 1, 0], [1, 1, 0, 1, 0, 0, 0, 0], [1, 1, 0, 1, 0, 0, 1, 0], [1, 1, 0, 1, 0, 1, 0, 0],
             [1, 1, 0, 1, 1, 0, 0, 0], [1, 1, 0, 1, 1, 0, 1, 0], [1, 1, 0, 1, 1, 1, 0, 0], [1, 1, 1, 0, 0, 0, 0, 0],
             [1, 1, 1, 0, 0, 0, 1, 0], [1, 1, 1, 0, 0, 1, 0, 0], [1, 1, 1, 0, 0, 1, 1, 0], [1, 1, 1, 0, 1, 0, 0, 0],
             [1, 1, 1, 0, 1, 0, 1, 0], [1, 1, 1, 0, 1, 1, 0, 0], [1, 1, 1, 1, 0, 0, 0, 0], [1, 1, 1, 1, 0, 0, 1, 0],
             [1, 1, 1, 1, 0, 1, 0, 0], [1, 1, 1, 1, 0, 1, 1, 0], [1, 1, 1, 1, 1, 0, 0, 0], [1, 1, 1, 1, 1, 0, 1, 0],
             [1, 1, 1, 1, 1, 1, 0, 0], [1, 1, 1, 1, 1, 1, 1, 0]]
        ]
        self._last_bits_size: int = -1
        self._substring_bits_length: int = -1

    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray):
        b_template: np.ndarray = np.array(random.choice(random.choice(self._templates)))
        if self._last_bits_size == -1 or self._last_bits_size != sequence.size:
            substring_bits_length: int = int(sequence.size // self._blocks_number)
            self._last_bits_size = sequence.size
            self._substring_bits_length = substring_bits_length
        else:
            substring_bits_length: int = self._substring_bits_length
        matches: np.ndarray = np.zeros(self._blocks_number, dtype=int)
        for i in range(self._blocks_number):
            block: np.ndarray = sequence[i * substring_bits_length:(i + 1) * substring_bits_length]
            position: int = 0
            count: int = 0
            while position < (substring_bits_length - b_template.size):
                if (block[position:position + b_template.size] == b_template).all():
                    position += b_template.size
                    count += 1
                else:
                    position += 1
            matches[i] = count
        mu: float = float(substring_bits_length - b_template.size + 1) / float(2 ** b_template.size)
        sigma: float = substring_bits_length * ((1.0 / float(2 ** b_template.size)) - (
                float((2 * b_template.size) - 1) / float(2 ** (2 * b_template.size))))
        chi_square: float = float(np.sum(((matches[:] - mu) ** 2) / sigma))
        if chi_square != 0:
            score: float = scipy.special.gammaincc(self._blocks_number / 2.0, chi_square / 2.0)
            if score >= self.threshold:
                return GeneratorTestResult(True, score)
            return GeneratorTestResult(False, score)
