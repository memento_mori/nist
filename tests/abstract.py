from abc import ABC, abstractmethod

import numpy as np

from .result import GeneratorTestResult


class AbstractTest(ABC):

    @property
    @abstractmethod
    def threshold(self) -> float:
        pass

    @abstractmethod
    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        pass

    @property
    def required_length(self) -> int:
        return 1000
