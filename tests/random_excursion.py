import numpy as np
import scipy.special

from .abstract import AbstractTest
from .result import GeneratorTestResult


class RandomExcursion(AbstractTest):

    def __init__(self):
        self._probabilities_xk = [
            np.array([0.5, 0.25, 0.125, 0.0625, 0.0312, 0.0312]),
            np.array([0.75, 0.0625, 0.0469, 0.0352, 0.0264, 0.0791]),
            np.array([0.8333, 0.0278, 0.0231, 0.0193, 0.0161, 0.0804]),
            np.array([0.875, 0.0156, 0.0137, 0.012, 0.0105, 0.0733]),
            np.array([0.9, 0.01, 0.009, 0.0081, 0.0073, 0.0656]),
            np.array([0.9167, 0.0069, 0.0064, 0.0058, 0.0053, 0.0588]),
            np.array([0.9286, 0.0051, 0.0047, 0.0044, 0.0041, 0.0531])
        ]

    @property
    def threshold(self) -> float:
        return 0.01

    def run(self, sequence: np.ndarray) -> GeneratorTestResult:
        sequence_copy: np.ndarray = sequence.copy()
        sequence_copy[sequence_copy == 0] = -1
        sum_prime: np.ndarray = np.concatenate(
            (np.array([0]), np.cumsum(sequence_copy), np.array([0]))).astype(int)
        cycles: [] = []
        cycle: [] = [0]
        for index, _ in enumerate(sum_prime[1:]):
            if sum_prime[index] != 0:
                cycle += [sum_prime[index]]
            else:
                cycle += [0]
                cycles.append(cycle)
                cycle: [] = [0]
        cycles.append(cycle)
        cycles_size: int = len(cycles)
        frequencies_table: dict = {
            -4: np.zeros(6, dtype=int),
            -3: np.zeros(6, dtype=int),
            -2: np.zeros(6, dtype=int),
            -1: np.zeros(6, dtype=int),
            1: np.zeros(6, dtype=int),
            2: np.zeros(6, dtype=int),
            3: np.zeros(6, dtype=int),
            4: np.zeros(6, dtype=int),
        }
        for value in frequencies_table.keys():
            for k in range(frequencies_table[value].size):
                count: int = 0
                for cycle in cycles:
                    occurrences: int = np.count_nonzero(np.array(cycle) == value)
                    if 5 > k == occurrences:
                        count += 1
                    elif occurrences >= 5:
                        count += 1
                frequencies_table[value][k] = count
        scores: [] = []
        for value in frequencies_table.keys():
            chi_square: float = np.sum(
                ((frequencies_table[value][:] - (cycles_size * (self._probabilities_xk[abs(value) - 1][:]))) ** 2) / (
                        cycles_size * self._probabilities_xk[abs(value) - 1][:]))
            score: float = scipy.special.gammaincc(5.0 / 2.0, chi_square / 2.0)
            scores.append(score)
        if all(score >= self.threshold for score in scores):
            return GeneratorTestResult(True, scores)
        return GeneratorTestResult(False, scores)
