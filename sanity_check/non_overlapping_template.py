import numpy as np

from tests import NonOverlappingTemplateMatchingTest

if __name__ == '__main__':
    test = NonOverlappingTemplateMatchingTest()
    seq = np.array([0, 0, 0, 0, 0, 0, 0, 0, 1])
    res = test.run(seq)
    print(f'Value: {res.result}, passed: {res.passes}')
