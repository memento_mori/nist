import numpy as np

from tests import OverlappingTemplateMatchingTest

if __name__ == '__main__':
    test = OverlappingTemplateMatchingTest()
    seq = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1])
    res = test.run(seq)
    print(f'Value: {res.result}, passed: {res.passes}')
