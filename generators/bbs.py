from typing import Iterator

import numpy as np

from .abstract import AbstractGenerator


class BBSGenerator(AbstractGenerator):
    N = 10941738641570527421809707322040357612003732945449205990913842131476349984288934784717997257891267332497625752899781833797076537244027146743531593354333897

    def _iterator(self, seed: int):
        current: int = (seed ** 2) % self.N

        while True:
            current = (current ** 2) % self.N
            bin_current = bin(current)
            lsb: int = int(bin_current[-1])
            yield lsb

    def get_sequence(self, seed: int, length: int) -> np.ndarray:
        iterator: Iterator[int] = self._iterator(seed)

        return np.array([next(iterator) for _ in range(length)])
