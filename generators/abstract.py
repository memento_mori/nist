from abc import ABC, abstractmethod

import numpy as np


class AbstractGenerator(ABC):

    @abstractmethod
    def get_sequence(self, seed: int, length: int) -> np.ndarray:
        pass
