from typing import Iterator, List

import numpy as np

from .abstract import AbstractGenerator


class XORGenerator(AbstractGenerator):
    INIT_STATE: str = '0001011011011001000101111001001010011011101101000100000010101111111010100100001010110110000000000100110000101110011111111100111'
    INT_INIT_STATE: List[int] = [int(item) for item in INIT_STATE]

    def _iterator(self):
        while True:
            current: int = self.INT_INIT_STATE[-1] ^ self.INT_INIT_STATE[-127]
            self.INT_INIT_STATE.append(current)
            yield current

    def get_sequence(self, seed: int, length: int) -> np.ndarray:
        iterator: Iterator[int] = self._iterator()
        return np.array([next(iterator) for _ in range(length)])
