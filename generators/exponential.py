from typing import Iterator

import numpy as np

from .abstract import AbstractGenerator


class ExponentialGenerator(AbstractGenerator):
    P_HEX: str = '987b6a6bf2c56a97291c445409920032499f9ee7ad128301b5d0254aa1a9633fdbd378d40149f1e23a13849f3d45992f5c4c6b7104099bc301f6005f9d8115e1'
    G_HEX: str = '3844506a9456c564b8b8538e0cc15aff46c95e69600f084f0657c2401b3c244734b62ea9bb95be4923b9b7e84eeaf1a224894ef0328d44bc3eb3e983644da3f5'
    P: int = int(P_HEX, 16)
    G: int = int(G_HEX, 16)

    def _iterator(self, seed: int):
        current: int = pow(self.G, seed, self.P)

        while True:
            current = pow(self.G, current, self.P)
            bin_current = bin(current)
            for bit in bin_current[2:]:
                yield int(bit)

    def get_sequence(self, seed: int, length: int) -> np.ndarray:
        iterator: Iterator[int] = self._iterator(seed)

        return np.array([next(iterator) for _ in range(length)])


class ExponentialGeneratorLSB(ExponentialGenerator):
    def _iterator(self, seed: int):
        current: int = pow(self.G, seed, self.P)

        while True:
            current = pow(self.G, current, self.P)
            bin_current = bin(current)
            lsb: int = int(bin_current[-1])
            yield lsb


class ExponentialGeneratorMSB(ExponentialGenerator):
    def _iterator(self, seed: int):
        current: int = pow(self.G, seed, self.P)

        while True:
            current = pow(self.G, current, self.P)
            bin_current = bin(current)
            msb: int = int(bin_current[2])
            yield msb
