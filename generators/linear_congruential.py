from typing import Iterator

import numpy as np

from .abstract import AbstractGenerator


class LinearCongruentialGenerator(AbstractGenerator):
    M: int = 2 ** 512
    A: int = 25214903917
    B: int = 11

    def _iterator(self, seed: int):
        current: int = seed

        while True:
            current = (self.A * current + self.B) % self.M
            bin_current = bin(current)
            for bit in bin_current[2:]:
                yield int(bit)

    def get_sequence(self, seed: int, length: int) -> np.ndarray:
        iterator: Iterator[int] = self._iterator(seed)

        return np.array([next(iterator) for _ in range(length)])


class LinearCongruentialGeneratorLowerLSB(LinearCongruentialGenerator):

    def _iterator(self, seed: int):
        current: int = seed

        while True:
            current = (self.A * current + self.B) % self.M
            bin_current = bin(current)
            lsb: int = int(bin_current[-1])
            yield lsb


class LinearCongruentialGeneratorLowerMSB(LinearCongruentialGenerator):

    def _iterator(self, seed: int):
        current: int = seed

        while True:
            current = (self.A * current + self.B) % self.M
            bin_current = bin(current)
            msb: int = int(bin_current[2])
            yield msb
