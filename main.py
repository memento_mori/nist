import numpy as np

from generators import (AbstractGenerator, BBSGenerator, ExponentialGenerator,
                        ExponentialGeneratorLSB, ExponentialGeneratorMSB,
                        LinearCongruentialGenerator,
                        LinearCongruentialGeneratorLowerLSB,
                        LinearCongruentialGeneratorLowerMSB, XORGenerator)
from tests import (AbstractTest, BinaryMatrixRankTest, BlockFrequency,
                   CumulativeSum, EntropyTest, GeneratorTestResult,
                   MaurersUniversalTest, MonobitTest,
                   NonOverlappingTemplateMatchingTest,
                   OverlappingTemplateMatchingTest, RandomExcursion,
                   RandomExcursionVariant, RunsInBlock, RunsTest, SerialTest,
                   SpectralTest)

GENERATORS = {
    'linear_congruential': LinearCongruentialGenerator,
    'linear_congruential_lsb': LinearCongruentialGeneratorLowerLSB,
    'linear_congruential_msb': LinearCongruentialGeneratorLowerMSB,
    'exponential': ExponentialGenerator,
    'exponential_lsb': ExponentialGeneratorLSB,
    'exponential_msb': ExponentialGeneratorMSB,
    'xor': XORGenerator,
    'bbs': BBSGenerator
}

TESTS = {
    '(1) monobit': MonobitTest,
    '(2) block_frequency': BlockFrequency,
    '(3) runs_test': RunsTest,
    '(4) runs_in_block': RunsInBlock,
    '(5) binary_matrix_rank': BinaryMatrixRankTest,
    '(6) spectral': SpectralTest,
    '(7) non_overlapping': NonOverlappingTemplateMatchingTest,
    '(8) overlapping': OverlappingTemplateMatchingTest,
    '(9) maurers': MaurersUniversalTest,
    '(10) serial_test': SerialTest,
    '(11) entropy_test': EntropyTest,
    '(12) cumulative_sum': CumulativeSum,
    '(13) random_excursion': RandomExcursion,
    '(14) random_excursion_variant': RandomExcursionVariant,
}

if __name__ == '__main__':
    seed = 13
    for generator_name, generator_class in GENERATORS.items():
        generator_instance: AbstractGenerator = generator_class()
        for test_name, test_class in TESTS.items():
            test_instance: AbstractTest = test_class()
            sequence: np.ndarray = generator_instance.get_sequence(seed=seed, length=test_instance.required_length)
            test_result: GeneratorTestResult = test_instance.run(sequence)
            print(f'{generator_name}|{test_name}: {test_result.result}, passed: {test_result.passes}')

        print('-' * 72)
